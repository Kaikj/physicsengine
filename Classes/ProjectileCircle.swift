//
//  ProjectileCircle.swift
//  BubbleGame
//
//  Created by Sean Ho on 14/2/16.
//  Copyright © 2016 nus.cs3217.a0111163. All rights reserved.
//

import UIKit

public class ProjectileCircle: NSObject, CircleBody, Collidable {
    // MARK: CircleBody

    public var xSpeed: CGFloat
    public var ySpeed: CGFloat
    public var center: CGPoint
    public var radius: CGFloat

    public init(center: CGPoint, radius: CGFloat) {
        // All objects start out with not moving.
        xSpeed = Constants.atRest
        ySpeed = Constants.atRest
        self.center = center
        self.radius = radius
    }

    // MARK: Collidable

    public func isColliding(body: protocol<Collidable, RigidBody>) -> Bool {
        switch (body) {
        case let circleBody as CircleBody:
            // We consider the square of the distances since square root is
            // an expansive operation and square is much cheaper.
            let distance = PhyMath.squaredDistanceBetween(point: center,
                andPoint: circleBody.center)
            let radiiDistance = PhyMath.square(radius + circleBody.radius)

            // It is colliding if the distance between the centers are less
            // than the square of the sum of the 2 radii
            return distance <= radiiDistance
        case let wall as Wall:
            return abs(wall.center.x - center.x) <= radius
        case let ceiling as Ceiling:
            return abs(ceiling.center.y - center.y) <= radius
        default:
            // No such case, the 2 objects cannot interact so they are not colliding.
            return false
        }
    }

    // MARK: Methods

    public func launchAtAngle(angle: CGFloat) {
        xSpeed = -Constants.projectileSpeed * sin(angle)
        ySpeed = -Constants.projectileSpeed * cos(angle)
    }

    public func updateCenter(time: CGFloat) {
        center.x += xSpeed * time
        center.y += ySpeed * time
    }

    public func stop() {
        xSpeed = Constants.atRest
        ySpeed = Constants.atRest
    }

    public func reflectFromVerticalLine() {
        xSpeed *= Constants.oppositeDirection
    }
}
