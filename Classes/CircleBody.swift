//
//  CircleBody.swift
//  BubbleGame
//
//  Created by Sean Ho on 14/2/16.
//  Copyright © 2016 nus.cs3217.a0111163. All rights reserved.
//

import UIKit

public protocol CircleBody: RigidBody {
    var radius: CGFloat { get set }
}
