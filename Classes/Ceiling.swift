//
//  Ceiling.swift
//  BubbleGame
//
//  Created by Sean Ho on 14/2/16.
//  Copyright © 2016 nus.cs3217.a0111163. All rights reserved.
//

import UIKit

public class Ceiling: RigidBody, Collidable {
    // MARK: RigidBody

    public var xSpeed: CGFloat
    public var ySpeed: CGFloat
    public var center: CGPoint

    public init(center: CGPoint) {
        // All objects start out with not moving.
        xSpeed = 0
        ySpeed = 0
        self.center = center
    }

    // MARK: Collidable

    public func isColliding(body: protocol<Collidable, RigidBody>) -> Bool {
        if let circleBody = body as? CircleBody {
            return abs(center.y - circleBody.center.y) <= circleBody.radius
        }

        // No such case, the 2 objects cannot interact so they are not colliding.
        return false
    }
}