//
//  PhyMath.swift
//  BubbleGame
//
//  Created by Sean Ho on 14/2/16.
//  Copyright © 2016 nus.cs3217.a0111163. All rights reserved.
//

import UIKit

struct PhyMath {
    static func square(number: CGFloat) -> CGFloat {
        return number * number
    }

    static func squaredDistanceBetween(point point1: CGPoint,
        andPoint point2: CGPoint) -> CGFloat {
            let xDistance = point1.x - point2.x
            let yDistance = point1.y - point2.y

            return square(xDistance) + square(yDistance)
    }
}
