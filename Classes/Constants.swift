/**
 `Constants` contain all the physical constants needed for the
 program to work correctly
*/

import UIKit

struct Constants {

	static let atRest = CGFloat(0)
    static let oppositeDirection = CGFloat(-1)
    static let projectileSpeed = CGFloat(1000)
}