//
//  RigidBody.swift
//  BubbleGame
//
//  Created by Sean Ho on 14/2/16.
//  Copyright © 2016 nus.cs3217.a0111163. All rights reserved.
//

import UIKit

public protocol RigidBody {
    var xSpeed: CGFloat { get set }
    var ySpeed: CGFloat { get set }
    var center: CGPoint { get set }
}