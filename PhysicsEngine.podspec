Pod::Spec.new do |s|
  s.name         = "PhysicsEngine"
  s.version      = "0.0.1"
  s.summary      = "A simple physics engine implementation for a simple bubble puzzle game"
  s.homepage     = "https://bitbucket.org/Kaikj/physicsengine"
  s.license      = "MIT"
  s.author       = { "Kaikj" => "bluesword246@gmail.com" }
  s.platform     = :ios
  s.source       = { :git => "https://bitbucket.org/Kaikj/physicsengine.git" }
  s.source_files = "Classes"
end
